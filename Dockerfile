FROM gcc:11

WORKDIR /app

RUN ls -al
RUN apt-get update --yes
RUN apt install --yes cowsay
COPY ./run.sh ./run.sh
ENTRYPOINT bash ./run.sh